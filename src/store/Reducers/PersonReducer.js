import {personConstants} from '../../constants/PersonConstants';

const initialState = {
    person: {
        authenticated: false,
        token: 'xyz',
    },
};

const PersonReducer = (state = initialState, action) => {
    switch (action.type) {
        case personConstants.AUTHENTICATED: {
            return {
                ...state,
                // person: {
                //     ...state,
                authenticated: action.payload,
                // },
            }
        }
        default: {
            return {
                state,
            }
        }
    }
};

export default PersonReducer;
