import {personConstants} from "../../constants/PersonConstants";

export const authenticated = (payload) => {
    return {
        type: personConstants.AUTHENTICATED,
        payload: payload,
    };
};
