import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Accept'] = 'application/json';

export const springDemoAxios = axios.create({
    baseURL: window.env.SPRING_DEMO_SERVICE_URL,
});
