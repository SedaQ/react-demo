import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import personReducer from './store/Reducers/PersonReducer'
import {Provider} from 'react-redux';
import {BrowserRouter} from "react-router-dom";
import {combineReducers, createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';

const loggerMiddleware = createLogger();


const rootReducer = createStore(combineReducers({
        person: personReducer,
    }),
    applyMiddleware(
        loggerMiddleware
    )
);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={rootReducer}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
registerServiceWorker();
