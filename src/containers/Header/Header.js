import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
// import {NavLink, withRouter} from "react-router-dom";
import "./Header.css";
import Grid from "@material-ui/core/Grid";
import Menu from "@material-ui/core/Menu";
import CloudUploadIcon from '@material-ui/icons/Menu';
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {ContactMail, Home, PeopleAlt} from "@material-ui/icons";
import {withStyles} from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import {NavLink} from "react-router-dom";
import {withRouter} from "react-router-dom";

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        id="menu-appbar"
        elevation={2}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        }
    },
}))(MenuItem);

const useStyles = makeStyles(theme => ({
    menuButton: {
        flexGrow: 1,
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    headerItemsPart: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    sectionMenuHeader: {
        justifyContent: 'flex-end',
    },
    sectionDesktop: {
        flexGrow: 1,
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
}));

const Header = (props) => {
    const {history} = props;
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const isMenuOpen = Boolean(anchorEl);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuClick = (pageURL) => {
        history.push(pageURL);
        setAnchorEl(null);
    };

    const menuItems = [
        {
            menuTitle: "Úvod",
            pageURL: "/home",
            icon: <Home fontSize="small"/>
        },
        {
            menuTitle: "Osoby",
            pageURL: "/persons",
            icon: <PeopleAlt fontSize="small"/>
        },
        {
            menuTitle: "Vytvořit Osoby",
            pageURL: "/persons/createForm",
            icon: <PeopleAlt fontSize="small"/>
        },
        {
            menuTitle: "Kontakty",
            pageURL: "/contact",
            icon: <ContactMail fontSize="small"/>
        },
    ];
    // https://material-ui.com/components/app-bar/

    return (
        <AppBar position="static" color="inherit">
            <Toolbar className={classes.sectionMenuHeader}>
                <div className={classes.sectionDesktop}>
                    <Typography variant="h6" className={classes.title}>
                        Training
                    </Typography>
                    <Grid container spacing={6}>
                        <Grid className={classes.headerItemsPart} item>
                            <ul id="navMenu">
                                <li>
                                    <NavLink
                                        exact to={menuItems[0].pageURL}
                                        className="navLink"
                                        activeClassName="activeRoute"
                                    >
                                        {menuItems[0].menuTitle}
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        exact to={menuItems[1].pageURL}
                                        className="navLink"
                                        activeClassName="activeRoute"
                                    >
                                        {menuItems[1].menuTitle}
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        exact to={menuItems[2].pageURL}
                                        className="navLink"
                                        activeClassName="activeRoute"
                                    >
                                        {menuItems[2].menuTitle}
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        exact to={menuItems[3].pageURL}
                                        className="navLink"
                                        activeClassName="activeRoute"
                                    >
                                        {menuItems[3].menuTitle}
                                    </NavLink>
                                </li>
                            </ul>
                        </Grid>
                    </Grid>
                </div>
                <div className={classes.sectionMobile}>
                    <Button
                        variant="outlined"
                        color="primary"
                        aria-label="menu"
                        className={classes.button}
                        onClick={handleMenu}
                        startIcon={<CloudUploadIcon/>}
                    >
                        Menu
                    </Button>
                    <StyledMenu
                        id="customized-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={isMenuOpen}
                        onClose={() => setAnchorEl(null)}
                    >
                        {menuItems.map(menuItem => {
                            const {menuTitle, pageURL, icon} = menuItem;
                            return (
                                <StyledMenuItem onClick={() => handleMenuClick(pageURL)}>
                                    <ListItemIcon>
                                        {icon}
                                    </ListItemIcon>
                                    <ListItemText primary={menuTitle}/>
                                </StyledMenuItem>
                            )
                        })}
                    </StyledMenu>
                </div>
            </Toolbar>
        </AppBar>
    )
};
export default withRouter(Header);
