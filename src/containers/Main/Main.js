import React from 'react';
import PersonList from "../../components/Person/PersonList";
import {Redirect, Route, Switch} from "react-router-dom";
import Contact from "../../components/Contact/Contact";
import NotFound from "../../components/NotFound/NotFound";
import PersonCreate from "../../components/Person/PersonCreate/PersonCreate";

const Main = () => {
    return (
        <div>
            <Switch>
                <Route
                    exact
                    path="/"
                    render={() => {
                        return (
                            <Redirect to="/home"/>
                        )
                    }}
                />
                <Route exact path="/home" component={PersonList}/>
                <Route exact path="/persons" component={PersonList}/>
                <Route path="/persons/createForm" component={PersonCreate}/>
                <Route path="/contact" component={Contact}/>
                <Route component={NotFound}/>
                {/*<Route render={<div><h1>The requested page was not found</h1></div>}/>*/}
            </Switch>
            {/*<PersonList/>*/}
        </div>
    )
};
export default Main;
