import React, {Component} from 'react';
import './App.css';
import Header from "./Header/Header";
import Main from "./Main/Main";
import {withRouter} from "react-router-dom";
// JSX
// containers
// hoc -- high order component

class App extends Component {

    render() {
        return (
            <div className="App">
                <Header/>
                <Main/>
            </div>
        );
    }
}

export default withRouter(App);
