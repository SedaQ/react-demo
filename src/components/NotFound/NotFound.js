import React from 'react';

const NotFound = () => {
    return (
        <div>
            <h1>The requested page was not found.</h1>
        </div>
    )
};

export default NotFound;
