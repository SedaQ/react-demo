import React from 'react'
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {Email, Person} from "@material-ui/icons";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {springDemoAxios} from "../../../api/AxiosInstances";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";
import * as validator from "validator";

class PersonCreate extends React.Component {

    state = {
        firstName: {
            error: false,
            value: "",
        },
        surname: {
            error: false,
            value: "",
        },
        pwd: {
            error: false,
            value: "",
        },
        nickname: {
            error: false,
            value: "",
        },
        email: {
            error: false,
            value: "@",
        },
        loading: false,
        messageSent: false,
    };

    handleSubmit = (event) => {
        event.preventDefault();
        if (!this.validate()) {
            return;
        }
        this.setState({
            ...this.state,
            loading: true
        });
        const createPersonObj = {
            firstName: this.state.firstName.value,
            surname: this.state.surname.value,
            pwd: this.state.pwd.value,
            nickname: this.state.nickname.value,
            email: this.state.email.value,
        };
        springDemoAxios.post('/persons', createPersonObj)
            .then((response) => {
                this.setState({
                    ...this.state,
                    loading: false,
                    messageSent: true
                });
            }).catch((error) => {
            this.setState({
                ...this.state,
                loading: false,
                messageSent: false
            });
        })

    };

    validate() {
        let fields = new Set();

        // fields.add(isLength(this.state.surname, {min: 3, max: 20}));

        return !fields.has(false);
    }


    textFieldChangeHandler = (event) => {
        // const nam = `contactForm.${event.target.name}`;
        const nam = event.target.name;
        const val = event.target.value;
        this.setState({
            ...this.state,
            [nam]: {
                ...this.state[nam],
                value: val,
                error: false,
            }
        });
    };

    snackBarHandleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            ...this.state.messageSent,
            messageSent: !this.state.messageSent
        });
    };

    render() {
        let showProgressIndicator;
        if (this.state.loading) {
            showProgressIndicator = <CircularProgress color="secondary"/>
        }
        let messageSentNotification = (
            <Snackbar open={this.state.messageSent}
                      autoHideDuration={6000}
                      onClose={this.snackBarHandleClose}
                      anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}>
                <Alert severity="success"
                       onClose={this.snackBarHandleClose}>
                    <AlertTitle>Email úspěšně odeslán</AlertTitle>
                    <strong>Těšíme se na vzájemnou spolupráci!</strong>
                </Alert>
            </Snackbar>
        );

        return (
            <div>
                <Paper>
                    <Typography variant="h5" gutterBottom>
                        Kontaktujte nás
                    </Typography>
                    <form onSubmit={this.handleSubmit} id="contactUsForm">
                        <Grid container justify="center" container spacing={4} item xs={12}>
                            <Grid item xs={12} container spacing={1} alignItems="flex-end">
                                <Grid item xs={1}>
                                    <Person/>
                                </Grid>
                                <Grid item xs={11}>
                                    <TextField
                                        required
                                        id="nameContactForm"
                                        name="firstName"
                                        label="Vaše jméno"
                                        value={this.state.firstName.value}
                                        onChange={this.textFieldChangeHandler}
                                        helperText={this.state.firstName.error ? "Jméno musí být vyplněno" : ""}
                                        error={this.state.firstName.error}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container spacing={1} alignItems="flex-end">
                                <Grid item xs={1}>
                                    <Person/>
                                </Grid>
                                <Grid item xs={11}>
                                    <TextField
                                        required
                                        id="surnameContactForm"
                                        name="surname"
                                        label="Vaše příjmení"
                                        value={this.state.surname.value}
                                        onChange={this.textFieldChangeHandler}
                                        helperText={this.state.surname.error ? "Vaše příjmení být vyplněno" : ""}
                                        error={this.state.surname.error}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container spacing={1} alignItems="flex-end">
                                <Grid item xs={1}>
                                    <Person/>
                                </Grid>
                                <Grid item xs={11}>
                                    <TextField
                                        required
                                        id="nicknameContactForm"
                                        name="nickname"
                                        label="Váš nickname"
                                        value={this.state.nickname.value}
                                        onChange={this.textFieldChangeHandler}
                                        helperText={this.state.nickname.error ? "Váš nickname být vyplněn" : ""}
                                        error={this.state.nickname.error}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container spacing={1} alignItems="flex-end">
                                <Grid item xs={1}>
                                    <Person/>
                                </Grid>
                                <Grid item xs={11}>
                                    <TextField
                                        required
                                        id="pwdContactForm"
                                        name="pwd"
                                        label="Heslo"
                                        value={this.state.pwd.value}
                                        onChange={this.textFieldChangeHandler}
                                        helperText={this.state.pwd.error ? "Váše heslo musí být vyplněno" : ""}
                                        error={this.state.pwd.error}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12} container spacing={1} alignItems="flex-end">
                                <Grid item xs={1}>
                                    <Email/>
                                </Grid>
                                <Grid item xs={11}>
                                    <TextField
                                        required
                                        id="emailContactForm"
                                        name="email"
                                        label="Váš email"
                                        value={this.state.email.value}
                                        onChange={this.textFieldChangeHandler}
                                        helperText={this.state.email.error ? "Váš email musí být validní" : ""}
                                        error={this.state.email.error}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    color="primary"
                                    type="submit"
                                    form="contactUsForm"
                                    variant="contained">
                                    Odeslat zprávu
                                </Button>
                            </Grid>
                            <Grid item xs={12}>
                                {showProgressIndicator}
                            </Grid>
                            <Grid item xs={4}>
                                {messageSentNotification}
                            </Grid>
                        </Grid>
                    </form>
                </Paper>

            </div>
        )
    }
}

export default PersonCreate;
