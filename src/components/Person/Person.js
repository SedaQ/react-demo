import React from 'react';

const Person = (props) => {
    return (
        <div>
            <p>My name is: {props.name}</p>
            <p>My email is: {props.email}</p>
            <input type="text" onChange={props.change} defaultValue={props.email}/>
        </div>
    )
};

export default Person;
