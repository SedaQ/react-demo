import React from 'react';

const Person = (props) => {
    return (
        <div>
            <p>Person id: {props.id}</p>
            <p>Person email is: {props.email}</p>
            <p>Person nickname is: {props.nickname}</p>
            <p>Person firstName is: {props.firstName}</p>
            <p>Person birthday is: {props.birthday}</p>
            }
        </div>
    )
};

export default Person;
