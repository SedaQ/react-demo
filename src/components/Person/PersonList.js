import React, {Component} from 'react';
import Person from "./Person";
import CircularProgress from "@material-ui/core/CircularProgress";
import {personConstants} from '../../constants/PersonConstants';
import * as personActions from '../../store/Actions/PersonActions';
import {connect} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {springDemoAxios} from "../../api/AxiosInstances";
import PersonFullView from "./PersonFullView/PersonFullView";

class PersonList extends Component {

    state = {
        persons: [
            {
                id: 1,
                name: 'Pavel Seda212',
                email: 'pavelseda@email.cz',
            },
            {
                id: 2,
                name: 'Pavel Seda212',
                email: 'john.doe@seznam.cz',
                // address: {
                //     city: 'Praha',
                // },
            },
            {
                id: 3,
                name: 'Pavel Seda212',
                email: 'john.doe@seznam.cz',
                // address: {
                //     city: 'Praha',
                // },
            }
        ],
        person: {
            id: 0,
            email: '',
            nickname: '',
            firstName: '',
            birthday: '',
            address: {
                id: 0,
                city: '',
                street: '',
                houseNumber: '',
                zipCode: '',
            }
        },
        isLoading: false,
        isError: true,
    };

    componentWillMount() {
        // call API
        // axios
        // initialize the state...
        // uspesne... isLoading: false
        console.log("componentWillMount is called");
    }

    componentDidMount() {
        console.log("componentDidMount is called");
    }

    changeNameHandler = (event) => {
        // const personConst = Object.assign({}, this.state.person);
        const personConst = {...this.state.persons[0]};
        personConst.email = event.target.value;
        this.setState({
            ...this.state.persons[0],
            person: personConst
        })
    };

    showOrHidePersonsHandler = () => {
        console.log('showOrHidePersonsHandler is called');
        const showPersons = this.state.isLoading;
        this.setState({
            ...this.state,
            isLoading: !showPersons,
        })
    };

    deletePersonHandler = (id) => {
        console.log('deletePersonHandler: ' + id);
        const personsList = [...this.state.persons];
        const newPersonList = personsList.filter((person) => person.id !== id);
        // call API to delete person
        this.setState({
            ...this.state,
            persons: newPersonList,
        });
    };

    showsPersonsHandler = () => {
        this.setState({
            ...this.state,
            isLoading: true,
        });
        springDemoAxios.get('/persons/' + 5)
            .then((response) => {
                this.setState({
                    ...this.state,
                    person: response.data,
                    isLoading: false
                })
            }).catch((error) => {
            this.setState({
                ...this.state,
                isError: true,
                isLoading: false
            })
        })
    };

    render() {

        console.log("render is called");

        // button, kdyz na nej kliknu, tak zobrazim osoby v pripade, ze nejsou, a skryji, v pripade, ze jsou zobrazene
        //

        // AOP - Aspect Oriented Programming -- Logging

        // vytvorit tlacitko, na ktere kdyz kliknu, tak odstranim konkretni osobu z listu

        // 1 vytvoreni metody --- splice, filter (pro potreby deletu)
        // 2 integrace teto metody do tlacitka
        // 3 vhodne umisteni tohoto tlacitka

        let personsList = null;
        if (this.state.isLoading) {
            personsList = (
                <div>
                    {this.state.persons.map(person => {
                        return (
                            <Person
                                key={person.id}
                                change={(event) => this.changeNameHandler(event)}
                                name={person.email}
                                email={person.email}/>
                        )
                    })}
                </div>
            )
        }

        let personById = null;
        if (this.state.isLoading === false) {
            personById = (
                <PersonFullView
                    id={this.state.person.id}
                    name={this.state.person.name}
                    email={this.state.person.email}
                />
            )
        }

        return (
            <div>
                <h1> Muj Super Nadpis ! </h1>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={4}
                >
                    <Grid>

                        {this.props.person.authenticated ?
                            <React.Fragment>
                                <p>Jsem autentizovan</p>
                                <Button variant="contained"
                                        color="primary"
                                        onClick={() => this.props.personAuth(false)}>
                                    Odhlásit se..
                                </Button>

                                <div></div>
                                <br></br>
                                {/*<br></br> totally wrong approach always use div for that purposes*/}

                                <Button variant="contained"
                                        color="secondary"
                                        onClick={this.showsPersonsHandler}>Vrat osoby z API</Button>
                                {personById}

                                {/*<Button onClick={this.showOrHidePersonsHandler} variant="contained">*/}
                                {/*Show or Hide persons*/}
                                {/*</Button>*/}
                                {/*{personsList}*/}
                            </React.Fragment>
                            :
                            <React.Fragment>
                                <p> Nejsem autentizovan, okamzite se autentizuj!</p>
                                <Button variant="contained"
                                        color="primary"
                                        onClick={() => this.props.personAuth(true)}>
                                    Přihlásit se..
                                </Button>
                            </React.Fragment>
                        }
                    </Grid>
                </Grid>
            </div>
        );
    }
}

// map global store state to this component
const mapStateToProps = state => {
    return {
        person: state.person
    };
};

// invoke actions that manipulate with store
const mapDispatchToProps = dispatch => {
    return {
        personAuth: (authenticated) => dispatch(personActions.authenticated(authenticated)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonList);
