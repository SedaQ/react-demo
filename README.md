# Spring demo

# Build and Run (Using Docker)
Build the project:
```
docker build -t sedaq/training-react-frontend .
```
Run the project:
```
docker run --name training-react-frontend -it -p 8000:8000 sedaq/training-react-frontend
```
