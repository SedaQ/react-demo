const redux = require('redux');

const createStore = redux.createStore;
// empty... no reducers.. ///

// authenticated.....

const initialState = {
    person: {
        authenticated: false,
    },
    loginCount: 0,
};

// Reducer
const authReducer = (state = initialState, action) => {
    if (action.type === 'AUTHENTICATED') {
        return {
            ...state,
            person: {
                ...state,
                authenticated: action.payload,
            }
        }
    } else if (action.type === 'LOGIN_COUNT') {
        return {
            ...state,
            loginCount: state.loginCount + 1,
        }
    }
    return state;
};

const store = createStore(authReducer);
console.log(store.getState());

store.subscribe(() => {
    console.log('[Subscription', store.getState())
});

store.dispatch({type: 'AUTHENTICATED', payload: true});
store.dispatch({type: 'LOGIN_COUNT'});
store.dispatch({type: 'LOGIN_COUNT'});
store.dispatch({type: 'LOGIN_COUNT'});

console.log(store.getState());
